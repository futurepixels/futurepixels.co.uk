FROM node:14.16-buster-slim as install

ENV NODE_ENV=production
ENV npm_config_loglevel=warn
ENV CYPRESS_INSTALL_BINARY=0

WORKDIR /srv

COPY ./package.json ./package.json
COPY ./package-lock.json ./package-lock.json

RUN npm install --production=false


FROM node:14.16-buster-slim as build

ENV NODE_ENV=production
ENV npm_config_loglevel=warn

WORKDIR /srv

COPY . /srv
COPY --from=install /srv/node_modules ./node_modules

RUN npm run build


FROM node:14.16-buster-slim as run

ARG GID=1000
ARG UID=1000

USER ${UID}:${GID}

ENV NODE_ENV=production
ENV npm_config_loglevel=warn

WORKDIR /srv

COPY package.json ./package.json
COPY public ./public
COPY --from=build /srv/.next ./.next
COPY --from=build /srv/next.config.js ./next.config.js
COPY --from=install /srv/node_modules ./node_modules

EXPOSE 3000

CMD ["npm", "run", "start"]
