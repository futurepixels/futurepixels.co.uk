.SILENT: release

DOCKER := docker
DOCKER_COMPOSE := docker-compose
NODE := ${DOCKER_COMPOSE} run --rm node
NPM := ${NODE} npm
CURRENT_BRANCH := `git rev-parse --abbrev-ref HEAD`
VERSION := `date +%Y%m%d%H%M%S`

all: build install run

.PHONY: build install update

build:
	${DOCKER_COMPOSE} build

install:
	${NPM} install

update:
	${NPM} update


.PHONY: run restart logs logs-tail stop destroy-containers destroy-images

run:
	${DOCKER_COMPOSE} up -d --force-recreate app

restart:
	${DOCKER_COMPOSE} restart

logs:
	${DOCKER_COMPOSE} logs

logs-tail:
	${DOCKER_COMPOSE} logs -f

stop:
	${DOCKER_COMPOSE} stop

destroy-containers:
	${DOCKER_COMPOSE} rm

destroy-images:
	${DOCKER} images -qa --filter reference=futurepixelscouk* | xargs docker rmi


.PHONY: test-suite unit functional

test: unit functional

unit:
	${NPM} test

functional: run
	npm run test.specification


.PHONY: lint

lint:
	${NPM} run lint

.PHONY: release

release:
	@if [ ${CURRENT_BRANCH} = "main" ]; then\
		git tag ${VERSION} -m "${VERSION}"; \
		git push origin --tags; \
	fi

.PHONY: clean-docker clean-app clean

clean-docker: stop destroy-containers destroy-images

clean-app:
	rm -rf ./{.next,node_modules}

clean: clean-docker clean-app

