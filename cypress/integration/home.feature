Feature: Home page

  I want to see a friendly introduction

  Scenario: Opening the home page
    Given I open the home page
    Then I see "Welcome to futurepixels.co.uk" in the title
    And I should see "A place for my articles and more!" in the introduction
    And I should see "Comming soon..." after the introduction
