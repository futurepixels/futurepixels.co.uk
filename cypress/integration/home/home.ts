/// <reference types="cypress" />
import { Given, Then, And } from 'cypress-cucumber-preprocessor/steps'

Given(/I open the home page/, () => {
  cy.visit('/')
})

Then(/I see {string} in the title/, (title: string) => {
  cy.get('[data-qa="home"]').contains(title)
})

And(/I see {string} in the introduction/, (introduction: string) => {
  cy.get('[data-qa="home"] .title').contains(introduction)
})

And(/I see {string} after the introduction/, (comingSoon: string) => {
  cy.get('[data-qa="home"] .description').contains(comingSoon)
})
