module.exports = {
  moduleDirectories: ['node_modules'],
  testMatch: ['<rootDir>/src/**/*.test.(js|ts|tsx)'],
  collectCoverage: true,
  collectCoverageFrom: ['<rootDir>/src/**/*.(js|ts|tsx)'],
  coveragePathIgnorePatterns: [],
  setupFiles: ['<rootDir>/jest.env.js'],
  setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
    '.+\\.(svg)$': 'jest-transform-stub',
  },
  globals: {
    'ts-jest': {
      tsconfig: '<rootDir>/tsconfig.test.json',
    },
  },
  modulePaths: ['src'],
  restoreMocks: true,
}
