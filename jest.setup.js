const { configure } = require('enzyme')
const React16Adaptor = require('enzyme-adapter-react-16')

configure({ adapter: new React16Adaptor() })
