const nextEnv = require('next-env')

const withNextEnv = nextEnv({
  publicPrefix: 'PUBLIC_',
})

const config = withNextEnv({
  compress: true,
  pageExtensions: ['page.tsx', 'page.ts'],
  reactStrictMode: true,
})

module.exports = config
