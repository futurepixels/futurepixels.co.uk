/// <reference types="cypress" />
describe('The home page', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('should display the correct structure', () => {
    cy.get('[data-qa="home"]').should('exist')
    cy.get('[data-qa="home"] .title').should('exist')
    cy.get('[data-qa="home"] .description').should('exist')
  })
})
