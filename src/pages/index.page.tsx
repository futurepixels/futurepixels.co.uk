import React from 'react'
import Head from 'next/head'

const Home = (): React.ReactElement => {
  return (
    <div className="container">
      <Head>
        <title>Futurepixels</title>
        <meta
          name="description"
          content="Articles on general development and programming along with other findings..."
        />
        <link rel="icon" href="/logo.png" />
      </Head>

      <main className="main" data-qa="home">
        <h1 className="title">Welcome to futurepixels.co.uk</h1>
        <p className="description">A place for my articles and more!</p>
        <i>Coming soon...</i>
      </main>
    </div>
  )
}

export default Home
