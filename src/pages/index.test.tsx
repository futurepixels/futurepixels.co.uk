import React from 'react'
import { shallow } from 'enzyme'
import Home from 'pages/index.page'

describe('Home', () => {
  it('should render correctly', () => {
    expect(shallow(<Home />)).toMatchSnapshot()
  })
})
